package example.com.recyclerviewexample;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;

public class ExampleAdapter extends RecyclerView.Adapter<ExampleHolder> {
    private IMain.Presenter presenter;
    private List<Item> list;

    public ExampleAdapter(List<Item> list,IMain.Presenter presenter) {
        this.presenter = presenter;
        if (list == null){
            list = new ArrayList<>();
        }
        this.list = list;
    }

    @NonNull
    @Override
    public ExampleHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ExampleHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item,parent,false),parent.getContext(),presenter);
    }


    @Override
    public void onBindViewHolder(@NonNull ExampleHolder holder, int position) {
        holder.bind(position,list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void addItem(Item item){
        list.add(item);
        notifyDataSetChanged();
    }
    public void delete(int position){
        list.remove(position);
        notifyDataSetChanged();
    }

    public void read(List<Item> list){
        this.list = list;
        notifyDataSetChanged();
    }

}
