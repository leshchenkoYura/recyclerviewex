package example.com.recyclerviewexample;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainPresenter implements IMain.Presenter {
    private IMain.View view;

    public MainPresenter() {

    }

    @Override
    public void onStartView(IMain.View view) {
        this.view = view;
    }

    @Override
    public void onStopView() {
        view = null;
    }

    private List<Item> mock(){
        List<Item> list = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            int id = new Random().nextInt();
            list.add(new Item(id,String.valueOf(id)));
        }
        return list;
    }

    @Override
    public void init() {
        view.initAdapter(mock());
    }

    @Override
    public void delete(int position) {
        view.delete(position);
    }

    @Override
    public void read() {
        view.read(mock());
    }
}
