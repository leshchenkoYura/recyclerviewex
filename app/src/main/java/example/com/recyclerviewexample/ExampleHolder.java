package example.com.recyclerviewexample;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import example.com.recyclerviewexample.databinding.ItemBinding;

public class ExampleHolder extends RecyclerView.ViewHolder {
    private IMain.Presenter presenter;
    private Context context;
    private ItemBinding binding;


    public ExampleHolder(@NonNull View itemView, Context context, IMain.Presenter presenter) {
        super(itemView);
        this.context = context;
        this.presenter = presenter;
        binding = DataBindingUtil.bind(itemView);
    }

    public void bind(int position,Item item){
        if (item != null && item.getValue() != null && !item.getValue().isEmpty()){
            binding.setItem(item);
            binding.textView.setOnClickListener(v -> {
                binding.textView.setText("Click");
            });
            binding.textView.setOnLongClickListener(v ->{
                presenter.delete(position);
                return false;
            });
        }
    }
}
