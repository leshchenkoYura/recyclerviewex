package example.com.recyclerviewexample;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;

import java.util.List;

import example.com.recyclerviewexample.databinding.ActivityMainBinding;

public class MainActivity extends BaseActivity<ActivityMainBinding> implements IMain.View{
private IMain.Presenter  presenter;
private ExampleAdapter adapter;

    @Override
    protected void initView(@Nullable Bundle savedInstanceState) {
        presenter = new MainPresenter();
        getBinding().setEvent(presenter);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    protected void startView() {
        presenter.onStartView(this);
        presenter.init();
    }

    @Override
    protected void stopView() {
        presenter.onStopView();
    }

    @Override
    public void initAdapter(List<Item> list) {
        adapter = new ExampleAdapter(list,presenter);
        getBinding().rvExample.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        getBinding().rvExample.setAdapter(adapter);
    }

    @Override
    public void read(List<Item> list) {
        adapter.read(list);
    }

    @Override
    public void addItem(Item item) {
        adapter.addItem(item);
    }

    @Override
    public void delete(int position) {
        adapter.delete(position);
    }
}