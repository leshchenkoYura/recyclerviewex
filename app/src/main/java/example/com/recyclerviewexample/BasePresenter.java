package example.com.recyclerviewexample;

public interface BasePresenter<View> {
    void onStartView(View view);

    void onStopView();
}
