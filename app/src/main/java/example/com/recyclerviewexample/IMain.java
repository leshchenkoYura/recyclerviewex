package example.com.recyclerviewexample;

import java.util.List;

public class IMain {
    public interface View{
        void initAdapter(List<Item> list);

        void read(List<Item> list);

        void addItem(Item item);

        void delete(int position);
    }

   public interface Presenter extends BasePresenter<View>{
        void init();

        void delete(int position);

        void read();
    }
}
